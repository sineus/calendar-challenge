import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './Calendar.css'
import Event from './Event'
import Timeline from './Timeline'

class Calendar extends Component {

  static defaultProps = {
    scheduleConfig: {
      min: 0,
      max: 720
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      hours: props.hours,
      schedule: props.schedule,
      scheduleConfig: props.scheduleConfig
    }
  }

  renderEventWrapper() {
    if (this.state.schedule && this.state.schedule.length) {
      return this.state.schedule.map((event, index) =>
        <Event key={index} config={event} />
      )
    } else {
      return (
        <div>
          <p>You have not event today</p>
        </div>
      )
    }
  }

  render() {
    return (
      <div className="calendar-wrapper">
        <Timeline hours={this.state.hours} />
        <div className="calendar-event-wrapper" style={{ height: `${this.state.scheduleConfig.max}px` }}>
          <div className="calendar-event">
            {this.renderEventWrapper()}
          </div>
        </div>
      </div>
    )
  }
}

Calendar.PropTypes = {
  hours: PropTypes.array.isRequired,
  schedule: PropTypes.array.isRequired,
  scheduleConfig: PropTypes.shape({
    min: PropTypes.number,
    max: PropTypes.number
  })
}

export default Calendar