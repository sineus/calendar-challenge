import './layOutDay'

describe('test layOutDay factory', () => {

  const layOutDay = window.layOutDay;
  let entry = [
    { start: 30, end: 150, content: { title: 'React master class', location: 'Mountain View, San Francisco' } },
    { start: 540, end: 600, content: { title: 'Google afterwork', location: 'Mountain View, San Francisco' } },
    { start: 560, end: 620, content: { title: 'Make money', location: 'Las Vegas, Nevada' } },
    { start: 610, end: 670, content: { title: 'Fadio.it appointment', location: 'Vieux-port, Marseille' } }
  ]
  let schedule

  beforeEach(() => {

    //Initialize schedule
    schedule = layOutDay(entry)
  })

  it('Check if layOutDay exists in window object', () => {
    expect(window.layOutDay).toBeTruthy()
  })

  it(`Check if receive ${entry.length} events`, () => {
    expect(schedule.length).toEqual(entry.length)
  })

  it(`Check if each events have all required property`, () => {
    schedule.forEach((event) => {
      [
        'start',
        'end',
        'content.title',
        'content.location',
        'style.height',
        'style.width',
        'style.left',
        'style.top'
      ].forEach((property) => {
        expect(event).toHaveProperty(property)
      })
    })
  })

  afterEach(() => {

    //Check if entry doesn't change after 
    expect(entry).toEqual(entry)
  })
})