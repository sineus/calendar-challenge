import React from 'react'
import ReactDOM from 'react-dom'
import TestRenderer from 'react-test-renderer';
import ShallowRenderer from 'react-test-renderer/shallow';

import Event from './Event'

describe('Event', () => {

  const renderer = new ShallowRenderer()
  const config = {
    start: '09:00',
    end: '10:00',
    style: {
      top: '20px'
    },
    content: {
      title: 'test',
      location: 'test'
    }
  }

  renderer.render(<Event config={config} />)
  const result = renderer.getRenderOutput()

  it('should get correct template', () => {

    expect(result.type).toBe('div')
    expect(result).toEqual(
      <div className="calendar-event-item" style={{ "top": "20px" }}>
        <div className="calendar-event-item-inner">
          <h3>{config.content.title}</h3>
          <p>{config.content.location}</p>
        </div>
      </div>
    )
  })
})
