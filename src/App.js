import React, { Component } from 'react'
import Calendar from './Calendar'
import { hours } from './models/Hour'
import './layOutDay'
import './App.css'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      schedule: [],
      hours: hours
    }

    if (!window.layOutDay) {
      return
    }

    this.layOutDay = window.layOutDay;

    this.state.schedule = this.layOutDay([
      { start: 30, end: 150, content: { title: 'React master class', location: 'Mountain View, San Francisco' } },
      { start: 540, end: 600, content: { title: 'Google afterwork', location: 'Mountain View, San Francisco' } },
      { start: 560, end: 620, content: { title: 'Make money', location: 'Las Vegas, Nevada' } },
      { start: 610, end: 670, content: { title: 'Fadio.it appointment', location: 'Vieux-port, Marseille' } }
    ])
  }

  render() {
    return (
      <div className="App">
        <Calendar hours={this.state.hours} schedule={this.state.schedule} scheduleConfig={this.state.scheduleConfig} />
      </div>
    )
  }
}

export default App;
