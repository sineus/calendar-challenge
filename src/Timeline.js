import React, { Component } from 'react'
import './Timeline.css'

class Timeline extends Component {
  constructor(props) {
    super(props)
    this.state = {
      hours: props.hours
    }
  }

  render() {
    return (
      <div className="Timeline">
        <ul>
          {this.state.hours.map((time, index) => (
            <li className={time.half ? 'half' : ''} key={index}>{time.value} {time.half ? '' : <span>{time.period}</span>}</li>
          ))}
        </ul>
      </div>
    )
  }
}

export default Timeline