window.layOutDay = (() => {

  /**
   * Generate schedule with generated rows
   * @param {Array} rows 
   * @returns {Array} schedule
   */
  function generateSchedule(events, config) {
    let schedule = []

    //Generate rows with columns
    let rows = generateRows(events, config)

    rows.forEach(row => {
      row.forEach((column, index) => {
        column.forEach(event => {

          let width = 100 / row.length
          let offset = ((index * width) / 100) * 100

          //Add style for each events
          event.style = {
            top: `${event.start}px`,
            left: `${offset}%`,
            height: `${event.end - event.start}px`,
            width: `${width}%`
          }

          schedule.push(event)
        })
      })
    })

    return schedule;
  }

  /**
   * Generate rows and column of events
   * @param {array} events 
   */
  function generateRows(events, config) {

    //Sort events by start value
    events = events.sort((a, b) => a.start - b.start)

    let rows = []
    let previousRow
    let isPlace
    let isOverlap

    events.forEach(event => {

      //If event is not ignore this
      if (!checkValidEvent(event, config)) return

      //For first row insert one column with event
      if (!rows.length) return rows.push([[event]])

      //Get previous row
      previousRow = rows[rows.length - 1]

      //Check if event overlap any events
      isOverlap = checkEventOverlap(previousRow, event)

      //Place event into new row and column
      if (!isOverlap) return rows.push([[event]])

      //Check if event can be placed in last row on new column
      isPlace = checkPlacingEvent(previousRow, event)

      //If event can't be placed in previous row existing column, place event in previous row new column
      if (!isPlace) previousRow.push([event])
    })

    return rows
  }

  /**
   * Check if event overlap with any event
   * @param {Array} previousRow 
   * @param {Object} event 
   */
  function checkEventOverlap(previousRow, event) {
    return previousRow.some(column => (event.start < column[column.length - 1].end))
  }

  /**
   * Check placing event in current column
   * @param {Array} previousRow 
   * @param {Array} event 
   */
  function checkPlacingEvent(previousRow, event) {
    return previousRow.some(column => {
      return (event.start >= column[column.length - 1].end) ? column.push(event) : false
    })
  }

  /**
   * Check if event is valid
   * @param {Object} event 
   * @param {Object} config
   */
  function checkValidEvent(event, config) {
    if (event.start < config.min) {
      console.error(`event start must be greater than ${config.min}`)
      return false
    }

    if (event.end > config.max) {
      console.error(`event end must be less than ${config.max}`)
      return false
    }

    return true
  }

  /**
   * Return schedule data
   * @param {Array} events
   * @return {Array}
   */
  return ((events = [], config = {}) => {
    return generateSchedule(events, config)
  })
})()