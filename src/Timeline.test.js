import React from 'react'
import ReactDOM from 'react-dom'
import TestRenderer from 'react-test-renderer';
import ShallowRenderer from 'react-test-renderer/shallow';

import Timeline from './Timeline'
import { hours } from './models/Hour'

describe('Timeline', () => {

  const renderer = new ShallowRenderer()
  renderer.render(<Timeline hours={hours} />)
  const result = renderer.getRenderOutput()

  it('should get correct template', () => {

    let template = (
      <div className="Timeline">
        <ul>
          {hours.map((time, index) => (
            <li className={time.half ? 'half' : ''} key={index}>{time.value} {time.half ? '' : <span>{time.period}</span>}</li>
          ))}
        </ul>
      </div>
    )
    expect(result.type).toBe('div')
    expect(result).toEqual(template)
  })
})
