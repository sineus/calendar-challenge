import React, { Component } from 'react'

class Event extends Component {
  constructor(props) {
    super(props)
    this.state = {
      start: props.config.start,
      end: props.config.end,
      style: props.config.style,
      content: props.config.content
    }
  }

  render() {
    return (
      <div className="calendar-event-item" style={this.state.style}>
        <div className="calendar-event-item-inner">
          <h3>{this.state.content.title}</h3>
          <p>{this.state.content.location}</p>
        </div>
      </div>
    )
  }
}

export default Event