import React from 'react'
import { shallow } from 'enzyme'

import App from './App'
import Calendar from './Calendar'
import { hours } from './models/Hour'
import './layOutDay'

describe('App', () => {

  let entry = [
    { start: 30, end: 150, content: { title: 'React master class', location: 'Mountain View, San Francisco' } },
    { start: 540, end: 600, content: { title: 'Google afterwork', location: 'Mountain View, San Francisco' } },
    { start: 560, end: 620, content: { title: 'Make money', location: 'Las Vegas, Nevada' } },
    { start: 610, end: 670, content: { title: 'Fadio.it appointment', location: 'Vieux-port, Marseille' } }
  ]
  let schedule = []

  beforeEach(() => {

    if (!window.layOutDay) return;

    //Initialize schedule
    schedule = layOutDay(entry)
  })

  it('renders without crashing', () => {
    shallow(<App />)
  })

  it('renders Calendar', () => {
    const state = {
      hours: hours,
      schedule: schedule
    }
    const wrapper = shallow(<App />)
    const calendar = <Calendar hours={state.hours} schedule={state.schedule} />
    expect(wrapper).toContainReact(calendar)
  })
})
